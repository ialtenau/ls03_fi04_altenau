package view.menue;

import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.LevelController;
import controller.TargetController;
import model.Level;
import model.User;

@SuppressWarnings("serial")
public class LeveldesignWindow extends JFrame {

	private LevelController lvlControl;
	private JPanel contentPane;
	private LevelChooser cardChooseLevel;
	private LevelEditor cardLevelEditor;
	private TargetController targetControl;
	private User user;
	private CardLayout cards;
	private String aktion;

	/**
	 * Create the frame.
	 */
	public LeveldesignWindow(AlienDefenceController alienDefenceController, User user, String aktion) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.targetControl = alienDefenceController.getTargetController();
		this.setUser(user);
		this.setAktion(aktion);

		setTitle("Leveldesigner");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.BLACK);
		setContentPane(contentPane);
		this.cards = new CardLayout();
		contentPane.setLayout(cards);

		this.cardChooseLevel = new LevelChooser(alienDefenceController, this);
		cardChooseLevel.setBackground(Color.BLACK);
		contentPane.add(cardChooseLevel, "levelChooser");

		this.cardLevelEditor = new LevelEditor(this, lvlControl, targetControl, Level.getDefaultLevel());
		contentPane.add(cardLevelEditor, "levelEditor");

		this.showLevelChooser();
		this.setVisible(true);
	}

	/**
	 * display leveleditor with a new level
	 */
	public void startLevelEditor() {
		this.cardLevelEditor.setLvl(this.lvlControl.createLevel());
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * disply leveleditor with a new level
	 * 
	 * @param level_id
	 */
	public void startLevelEditor(int level_id) {
		this.cardLevelEditor.setLvl(this.lvlControl.readLevel(level_id));
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * display a jTable with all Levels
	 */
	public void showLevelChooser() {
		this.cards.show(contentPane, "levelChooser");
		this.cardChooseLevel.updateTableData();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAktion() {
		return aktion;
	}

	public void setAktion(String aktion) {
		this.aktion = aktion;
	}

}
