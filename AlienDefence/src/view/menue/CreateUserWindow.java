package view.menue;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.UserController;
import model.User;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;

@SuppressWarnings("serial")
public class CreateUserWindow extends JFrame {
	
	private UserController userController;
	private JPanel contentPane;
	private JTextField textFirstName;
	private JTextField textSurName;
	private JTextField textBirthday;
	private JTextField textStreet;
	private JTextField textHouseNumber;
	private JTextField textPostalCode;
	private JTextField textCity;
	private JTextField textLoginName;
	private JTextField textPassword;
	private JTextField textSalaryExpectations;
	private JTextField textMaritalStatus;
	private JTextField textFinalGrade;
	private JFrame frame;
	
		
	public CreateUserWindow(UserController userController) {
		this.userController = userController;
		createPane();
		
		JFrame frame = new JFrame("Create User");
        frame.setContentPane(this.contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        this.frame = frame;
	}
	
	public void createPane() {
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Create User");
		lblNewLabel.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setForeground(Color.WHITE);
		contentPane.add(lblFirstName);
		
		textFirstName = new JTextField();
		contentPane.add(textFirstName);
		textFirstName.setColumns(10);
		
		JLabel lblSurName = new JLabel("Last Name");
		lblSurName.setForeground(Color.WHITE);
		contentPane.add(lblSurName);
		
		textSurName = new JTextField();
		contentPane.add(textSurName);
		textSurName.setColumns(10);
		
		JLabel lblBirthday = new JLabel("Birthday");
		lblBirthday.setForeground(Color.WHITE);
		contentPane.add(lblBirthday);
		
		textBirthday = new JTextField();
		contentPane.add(textBirthday);
		textBirthday.setColumns(10);
		
		JLabel lblStreet = new JLabel("Street");
		lblStreet.setForeground(Color.WHITE);
		contentPane.add(lblStreet);
		
		textStreet = new JTextField();
		contentPane.add(textStreet);
		textStreet.setColumns(10);
		
		JLabel lblHouseNumber = new JLabel("House Number");
		lblHouseNumber.setForeground(Color.WHITE);
		contentPane.add(lblHouseNumber);
		
		textHouseNumber = new JTextField();
		contentPane.add(textHouseNumber);
		textHouseNumber.setColumns(10);
		
		JLabel lblPostalCode = new JLabel("Postal Code");
		lblPostalCode.setForeground(Color.WHITE);
		contentPane.add(lblPostalCode);
		
		textPostalCode = new JTextField();
		contentPane.add(textPostalCode);
		textPostalCode.setColumns(10);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setForeground(Color.WHITE);
		contentPane.add(lblCity);
		
		textCity = new JTextField();
		contentPane.add(textCity);
		textCity.setColumns(10);
		
		JLabel lblLoginname = new JLabel("Loginname");
		lblLoginname.setForeground(Color.WHITE);
		contentPane.add(lblLoginname);
		
		textLoginName = new JTextField();
		contentPane.add(textLoginName);
		textLoginName.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(Color.WHITE);
		contentPane.add(lblPassword);
		
		textPassword = new JTextField();
		contentPane.add(textPassword);
		textPassword.setColumns(10);
		
		JLabel lblSalaryExpectations = new JLabel("Salary Expecations");
		lblSalaryExpectations.setForeground(Color.WHITE);
		contentPane.add(lblSalaryExpectations);
		
		textSalaryExpectations = new JTextField();
		contentPane.add(textSalaryExpectations);
		textSalaryExpectations.setColumns(10);
		
		JLabel lblMaritalStatus = new JLabel("Martial Status");
		lblMaritalStatus.setForeground(Color.WHITE);
		contentPane.add(lblMaritalStatus);
		
		textMaritalStatus = new JTextField();
		contentPane.add(textMaritalStatus);
		textMaritalStatus.setColumns(10);
		
		JLabel lblFinalGrade = new JLabel("Final Grade");
		lblFinalGrade.setForeground(Color.WHITE);
		contentPane.add(lblFinalGrade);
		
		textFinalGrade = new JTextField();
		contentPane.add(textFinalGrade);
		textFinalGrade.setColumns(10);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (validateUser()) {
					User user = new User(
						0,
						textFirstName.getText(),
                        textSurName.getText(),
                        LocalDate.parse(textBirthday.getText()),
                        textStreet.getText(),
                        textHouseNumber.getText(),
                        textPostalCode.getText(),
                        textCity.getText(),
                        textLoginName.getText(),
                        textPassword.getText(),
                        Integer.parseInt(textSalaryExpectations.getText()),
                        textMaritalStatus.getText(),
                        Double.parseDouble(textFinalGrade.getText())
					);
					
					userController.createUser(user);
					frame.dispose();
				}
			}
		});
		
		contentPane.add(btnCreate);
	}
	
	public boolean validateUser() {
		String[] userData = {
	        textFirstName.getText(),
	        textSurName.getText(),
	        textBirthday.getText(),
	        textStreet.getText(),
	        textHouseNumber.getText(),
	        textPostalCode.getText(),
	        textCity.getText(),
	        textLoginName.getText(),
	        textPassword.getText(),
	        textSalaryExpectations.getText(),
	        textMaritalStatus.getText(),
	        textFinalGrade.getText()
		};
		
		for(String userDataEntry : userData) {
			if (userDataEntry == null || userDataEntry.isEmpty()) {
				return false;
			}
		}
		
		return true;
	}
	
}
