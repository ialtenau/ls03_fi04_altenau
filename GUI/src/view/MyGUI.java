package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTextToChange = new JLabel("Dieser Text soll verändert werden.");
		lblTextToChange.setBounds(55, 6, 352, 16);
		lblTextToChange.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTextToChange);
		
		JLabel lblTaskChangeBackgroundColor = new JLabel("Augabe 1: Hintergrundfarbe ändern");
		lblTaskChangeBackgroundColor.setBounds(6, 45, 438, 16);
		contentPane.add(lblTaskChangeBackgroundColor);
		
		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_click();
			}
		});
		btnRed.setBounds(16, 72, 117, 29);
		contentPane.add(btnRed);
		
		JButton btnGreen = new JButton("Grün");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_click();
			}
		});
		btnGreen.setBounds(170, 73, 117, 29);
		contentPane.add(btnGreen);
		
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_click();
			}
		});
		btnBlue.setBounds(312, 72, 117, 29);
		contentPane.add(btnBlue);
		
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_click();
			}
		});
		btnYellow.setBounds(16, 102, 117, 29);
		contentPane.add(btnYellow);
		
		JButton btnDefaultColor = new JButton("Standardfarbe");
		btnDefaultColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonDefault_click();
			}
		});
		btnDefaultColor.setBounds(170, 102, 117, 29);
		contentPane.add(btnDefaultColor);
		
		JButton btnChooseColor = new JButton("Farbe wählen");
		btnChooseColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonChooseColor_click();
			}
		});
		// ToDo
		btnChooseColor.setBounds(312, 102, 117, 29);
		contentPane.add(btnChooseColor);
		
		JLabel lblTaskChangeText = new JLabel("Aufgabe 2: Text verändern");
		lblTaskChangeText.setBounds(6, 143, 438, 16);
		contentPane.add(lblTaskChangeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setFont(new Font("Arial", Font.PLAIN, lblTextToChange.getFont().getSize()));
			}
		});
		btnArial.setBounds(16, 165, 117, 29);
		contentPane.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setFont(new Font("Comic Sans MS", Font.PLAIN, lblTextToChange.getFont().getSize()));
			}
		});
		btnComicSans.setBounds(170, 165, 117, 29);
		contentPane.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setFont(new Font("Courier New", Font.PLAIN, lblTextToChange.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(312, 165, 117, 29);
		contentPane.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(16, 199, 413, 26);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnWriteLabel = new JButton("Ins Label schreiben");
		btnWriteLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setText(txtHierBitteText.getText());
			}
		});
		btnWriteLabel.setBounds(16, 237, 212, 29);
		contentPane.add(btnWriteLabel);
		
		JButton btnDeleteLabel = new JButton("Text im Label löschen");
		btnDeleteLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setText("");
			}
		});
		btnDeleteLabel.setBounds(238, 237, 191, 29);
		contentPane.add(btnDeleteLabel);
		
		JLabel lblTaskChangeTextColor = new JLabel("Aufgabe 3: Textfarbe ändern");
		lblTaskChangeTextColor.setBounds(6, 278, 438, 16);
		contentPane.add(lblTaskChangeTextColor);
		
		JButton btnRedText = new JButton("Rot");
		btnRedText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setForeground(Color.red);
			}
		});
		btnRedText.setBounds(16, 306, 117, 29);
		contentPane.add(btnRedText);
		
		JButton btnBlueText = new JButton("Blau");
		btnBlueText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setForeground(Color.blue);
			}
		});
		btnBlueText.setBounds(170, 306, 117, 29);
		contentPane.add(btnBlueText);
		
		JButton btnBlackText = new JButton("Schwarz");
		btnBlackText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setForeground(Color.black);
			}
		});
		btnBlackText.setBounds(312, 306, 117, 29);
		contentPane.add(btnBlackText);
		
		JLabel lblTaskChangeFontSize = new JLabel("Aufgabe 4: Schriftgröße ändern");
		lblTaskChangeFontSize.setBounds(6, 347, 438, 16);
		contentPane.add(lblTaskChangeFontSize);
		
		JButton btnIncreaseFontSize = new JButton("+");
		btnIncreaseFontSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setFont(
						new Font(lblTextToChange.getFont().getName(), Font.PLAIN, lblTextToChange.getFont().getSize() + 1)
				);
			}
		});
		btnIncreaseFontSize.setBounds(16, 375, 212, 29);
		contentPane.add(btnIncreaseFontSize);
		
		JButton btnDecreaseFontSize = new JButton("-");
		btnDecreaseFontSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setFont(
						new Font(lblTextToChange.getFont().getName(), Font.PLAIN, lblTextToChange.getFont().getSize() - 1)
				);
			}
		});
		btnDecreaseFontSize.setBounds(238, 375, 191, 29);
		contentPane.add(btnDecreaseFontSize);
		
		JLabel lblTaskChangeTextAllignment = new JLabel("Aufgabe 5: Textausrichtung ändern");
		lblTaskChangeTextAllignment.setBounds(6, 416, 438, 16);
		contentPane.add(lblTaskChangeTextAllignment);
		
		JButton btnAllignLeft = new JButton("Linksbündig");
		btnAllignLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnAllignLeft.setBounds(16, 444, 117, 29);
		contentPane.add(btnAllignLeft);
		
		JButton btnAllignCenter = new JButton("Zentriert");
		btnAllignCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnAllignCenter.setBounds(170, 444, 117, 29);
		contentPane.add(btnAllignCenter);
		
		JButton btnAllignRight = new JButton("Rechtsbündig");
		btnAllignRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextToChange.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnAllignRight.setBounds(312, 444, 117, 29);
		contentPane.add(btnAllignRight);
		
		JLabel lblTaskEndProgram = new JLabel("Aufgabe 6: Programm beenden");
		lblTaskEndProgram.setBounds(6, 485, 438, 16);
		contentPane.add(lblTaskEndProgram);
		
		JButton btnExitProgram = new JButton("EXIT");
		btnExitProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonExitProgram_click();
			}
		});
		btnExitProgram.setBounds(16, 513, 413, 53);
		contentPane.add(btnExitProgram);
	}
	
	private void buttonRed_click() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.red);
	}
	
	private void buttonGreen_click() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.green);
	}
	
	private void buttonBlue_click() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.blue);
	}
	
	private void buttonYellow_click() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.yellow);
	}
	
	private void buttonDefault_click() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(null);
	}
	
	private void buttonChooseColor_click() {
		// TODO Auto-generated method stub
		Color chooseColor = JColorChooser.showDialog(null, null, null);
		this.contentPane.setBackground(chooseColor);
	}
	
	private void buttonExitProgram_click() {
		// TODO Auto-generated method stub
		System.exit(0);
	}
}
